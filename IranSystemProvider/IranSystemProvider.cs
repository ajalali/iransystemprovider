﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IranSystemProvider.Resources;
using System.Collections;

namespace IranSystemProvider
{
    public class IranSystemProvider
    {
        private IDictionaryMapper _characterMapper = null;
        public IranSystemProvider()
        {

        }

        #region Public Methods
        public string GetIranSystemText(string unicodeText)
        {
            Encoding encoder = Encoding.GetEncoding("windows-1256");
            byte[] iranSystemBytes = GetIranSystemBytes(unicodeText).ToArray<byte>();
            Array.Reverse(iranSystemBytes);
            string iranSystemText = encoder.GetString(iranSystemBytes);
            return iranSystemText;
        }

        public string GetUnicodeString(string iranSystemText)
        {
            throw new Exception("Not Yet emplemented");
        }
        #endregion

        #region Private Methods
        private IList<byte> GetIranSystemBytes(string unicodeText)
        {
            // " رشته ای که فارسی است را دو کاراکتر فاصله به ابتدا و انتهایآن اضافه می کنیم

            string unicodeString = " " + unicodeText + " ";
            //ایجاد دو انکدینگ متفاوت
            Encoding ascii = //Encoding.ASCII;
                Encoding.GetEncoding("windows-1256");

            Encoding unicode = Encoding.Unicode;

            // تبدیل رشته به بایت
            byte[] unicodeBytes = unicode.GetBytes(unicodeString);

            // تبدیل بایتها از یک انکدینگ به دیگری
            byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

            // Convert the new byte[] into a char[] and then into a string.
            char[] asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
            ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
            string asciiString = new string(asciiChars);
            byte[] b22 = Encoding.GetEncoding("windows-1256").GetBytes(asciiChars);


            int limit = b22.Length;

            byte pre = 0, cur = 0;


            IList<byte> IS_Result = new List<byte>();
            for (int i = 0; i < limit; i++)
            {

                if (IsLatingLetter(b22[i]))
                {
                    cur = GetLatingLetter(b22[i]);

                    IS_Result.Add(cur);


                    pre = cur;
                }
                else if (i != 0 && i != b22.Length - 1)
                {
                    cur = GetIranSystemCharacter(b22[i - 1], b22[i], b22[i + 1]);

                    if (cur == 145) // برای بررسی استثنای لا
                    {
                        if (pre == 243)
                        {
                            IS_Result.RemoveAt(IS_Result.Count - 1);
                            IS_Result.Add(242);
                        }
                        else
                        {
                            IS_Result.Add(cur);
                        }
                    }
                    else
                    {
                        IS_Result.Add(cur);
                    }



                    pre = cur;
                }

            }

            return IS_Result;
        }
        private bool IsLatingLetter(byte character)
        {
            return (character < 128 && character > 31);
        }
        private byte GetLatingLetter(byte character)
        {
            if ("0123456789".IndexOf((char)character) >= 0)
                return (byte)(character + 80);
            return GetPersioanExceptions(character);
        }
        private byte GetPersioanExceptions(byte character)
        {
            switch (character)
            {
                case (byte)'(': return (byte)')';
                case (byte)'{': return (byte)'}';
                case (byte)'[': return (byte)']';
                case (byte)')': return (byte)'(';
                case (byte)'}': return (byte)'{';
                case (byte)']': return (byte)'[';
                default: return (byte)character;

            }

        }
        private bool IsFinalLetter(byte character)
        {
            string s = "ءآأؤإادذرزژو";

            if (s.ToString().IndexOf((char)character) >= 0)
            {
                return true;

            }
            return false;
        }
        private bool IsWhiteLetter(byte character)
        {
            if (character == 8 || character == 09 || character == 10 || character == 13 || character == 27 || character == 32 || character == 0)
            {
                return true;
            }
            return false;
        }
        private bool CharCond(byte character)
        {
            return IsWhiteLetter(character)
                || IsLatingLetter(character)
                || character == 191;
        }
        private byte GetIranSystemCharacter(byte PreviousChar, byte CurrentChar, byte NextChar)
        {
            bool PFlag = CharCond(PreviousChar) || IsFinalLetter(PreviousChar);
            bool NFlag = CharCond(NextChar);
            if (PFlag && NFlag)
                _characterMapper = new CharacterMapperSepratedLetters();
            else if (PFlag)
                _characterMapper = new CharacterMapperStickyLetters();
            else if (NFlag)
                _characterMapper = new CharacterMapperFinalLetters();
            else
                _characterMapper = new ICharacterMapperMiddelLetters();
            return _characterMapper.CharacterMapper[CurrentChar];
        }
        #endregion
    }
}
