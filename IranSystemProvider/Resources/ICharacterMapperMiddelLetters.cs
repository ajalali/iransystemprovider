﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IranSystemProvider.Resources
{
    public class ICharacterMapperMiddelLetters : IDictionaryMapper
    {
        private readonly IDictionary<byte, byte> _characterMapper = null;
        public ICharacterMapperMiddelLetters()
        {
            _characterMapper = new Dictionary<byte, byte>
            {
                                {48 , 128}, // 0
                                {49 , 129}, // 1
                                {50 , 130}, // 2
                                {51 , 131}, // 3
                                {52 , 132}, // 4
                                {53 , 133}, // 5
                                {54 , 134}, // 6
                                {55 , 135}, // 7
                                {56 , 136}, // 8
                                {57 , 137}, // 9
                                {161, 138}, // ،
                                {191, 140}, // ؟
                                {193,143}, //
                                {194,141}, //
                                {195,145}, //
                                {196,248}, // 
                                {197,145}, // 
                                {198,254}, //
                                {199,145}, // 
                                {200,147}, // 
                                {201,250}, //
                                {202,151}, //
                                {203,153}, //
                                {204,155}, //
                                {205,159}, //
                                {206,161}, //
                                {207,162}, //
                                {208,163}, //
                                {209,164}, //
                                {210,165}, //
                                {211,168}, // 
                                {212,170}, //
                                {213,172}, //
                                {214,174}, //
                                {216,175}, // 
                                {217,224}, //
                                {218,227}, //
                                {219,231}, //
                                {220,139}, //
                                {221,234}, //
                                {222,236}, //
                                {223,238}, //
                                {225,243}, //
                                {227,245}, // 
                                {228,247}, //
                                {229,250}, //
                                {230,248}, //
                                {236,254}, //
                                {237,254}, // 
                                {129,149}, //
                                {141,157}, //
                                {142,166}, // 
                                {152,238}, // 
                                {144,240}, //
            };
        }
        public IDictionary<byte, byte> CharacterMapper { get { return _characterMapper; } }
    }
}