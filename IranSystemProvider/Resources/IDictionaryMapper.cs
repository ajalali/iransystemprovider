﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IranSystemProvider.Resources
{
    public interface IDictionaryMapper
    {
        IDictionary<byte, byte> CharacterMapper { get; }
    }
}
