﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IranSystemProvider.Resources
{
    public class CharacterMapperFinalLetters : IDictionaryMapper
    {
        private readonly IDictionary<byte, byte> _characterMapper = null;
        public CharacterMapperFinalLetters()
        {
            _characterMapper = new Dictionary<byte, byte>{
                                {48 , 128}, // 0
                                {49 , 129}, // 1
                                {50 , 130}, // 2
                                {51 , 131}, // 3
                                {52 , 132}, // 4
                                {53 , 133}, // 5
                                {54 , 134}, // 6
                                {55 , 135}, // 7
                                {56 , 136}, // 8
                                {57 , 137}, // 9
                                {161, 138}, // ،
                                {191, 140}, // ؟
                                {193, 143}, //
                                {194, 141}, //
                                {195, 145}, //
                                {196, 248}, //
                                {197, 145}, // 
                                {198, 252}, //
                                {199, 145}, // 
                                {200, 146}, // 
                                {201, 249}, //
                                {202, 150}, //
                                {203, 152}, // 
                                {204, 154}, //
                                {205, 158}, // 
                                {206, 160}, //
                                {207, 162}, //
                                {208, 163}, // 
                                {209, 164}, //
                                {210, 165}, //
                                {211, 167}, // 
                                {212, 169}, // 
                                {213, 171}, //
                                {214, 173}, // 
                                {216, 175}, // 
                                {217, 224}, //
                                {218, 226}, // 
                                {219, 230}, // 
                                {220, 139}, //
                                {221, 233}, // 
                                {222, 235}, //
                                {223, 237}, //
                                {225, 241}, // 
                                {227, 244}, //
                                {228, 246}, //
                                {229, 249}, //   
                                {230, 248}, // 
                                {236, 252}, //
                                {237, 252}, // 
                                {129, 148}, // 
                                {141, 156}, //
                                {142, 166}, // 
                                {152, 237}, // 
                                {144, 239}//
            };
        }

        public IDictionary<byte, byte> CharacterMapper { get { return _characterMapper; } }
    }
}
