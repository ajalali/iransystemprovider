﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IranSystemProvider.Resources
{
    public class CharacterMapperStickyLetters : IDictionaryMapper
    {
        private IDictionary<byte, byte> _characterMapper = null;
        public CharacterMapperStickyLetters()
        {
            _characterMapper = new Dictionary<byte, byte>{
                                   {48,128},//
                                   {49,129},//
                                   {50,130},
                                   {51,131},//
                                   {52,132},//
                                   {53,133},
                                   {54,134},//
                                   {55,135},//
                                   {56,136},
                                   {57,137},//
                                   {161,138},//،
                                   {191,140},//?
                                   {193,143},//ء
                                   {194,141},//آ
                                   {195,144},//أ
                                   {196,248},//ؤ
                                   {197,144},//إ
                                   {198,254},//ئ
                                   {199,144},//ا
                                   {200,147},//ب
                                   {201,251},//ة
                                   {202,151},//ت
                                   {203,153},//ث
                                   {204,155},//ج
                                   {205,159},//ح
                                   {206,161},//خ
                                   {207,162},//د
                                   {208,163},//ذ
                                   {209,164},//ر
                                   {210,165},//ز
                                   {211,168},//س
                                   {212,170},//ش
                                   {213,172},//ص
                                   {214,174},//ض
                                   {216,175},//ط
                                   {217,224},//ظ
                                   {218,228},//ع
                                   {219,232},//غ
                                   {220,139},//-
                                   {221,234},//ف
                                   {222,236},//ق
                                   {223,238},//ك
                                   {225,243},//ل
                                   {227,245},//م
                                   {228,247},//ن
                                   {229,251},//ه
                                   {230,248},//و
                                   {236,254},//ی
                                   {237,254},//ي
                                   {129,149},//پ
                                   {141 ,157},//چ
                                   {142,166},//ژ
                                   {152,238},//ک
                                   {144,240},//گ
                     };
        }
        public IDictionary<byte, byte> CharacterMapper { get { return _characterMapper; } }
    }
}
